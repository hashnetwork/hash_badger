# badger_dao

基于badger的数据访问

badger的官方地址：https://github.com/dgraph-io/badger

文档：https://dgraph.io/docs/badger/

功能：

在badger基础上，支持多个数据库并存的情况，同时支持内存数据库，acid事务和ttl功能

内存数据库的用法：

dao:=NewMemory()

硬盘数据库的用法：

option:=DefaultOption().withPath(basePath).withName(数据库名)

dao:=New(&option)

事务的用法：

// tx:=dao.NewHashKvReadTx() // 创建一个只读事务

tx:=dao.NewHashKvWriteTx() //创建一个读写事务

tx.Commit() //提交事务

tx.Discard()//放弃一个事务，在不确定commit是否执行的情况下一般discard方法在defer中执行，确保数据提交或者放弃。

//数据库关闭：

dao.Close()


