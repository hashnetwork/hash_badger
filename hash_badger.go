package hash_badger

import (
	"encoding/json"
	"errors"
	"github.com/dgraph-io/badger/v4"
	"os"
	"strings"
	"sync"
	"time"
)

const (
	SortAsc = "asc"
	SortDes = "desc"
)

type HashKvTx struct {
	Txn  *badger.Txn
	name string
}
type BadgerOption struct {
	Path      string
	DbName    string
	ClearOld  bool
	SyncWrite bool
}

//type MyWork func(myTx *MyTx) (interface{}, error)

var (
	dbMap      sync.Map
	globalLock sync.Mutex
)

//var dbMap map[string]*badger.DB

type BadgerKvDao struct {
	path string //路径
	name string //库名
}

func NewBadgerKvDao() *BadgerKvDao {
	return &BadgerKvDao{}
}

func DefaultOption() *BadgerOption {
	return &BadgerOption{
		Path:     "",
		DbName:   "no-name",
		ClearOld: false,
	}
}

func NewOptions(path string, dbName string, isMemory bool, clearOld bool) *BadgerOption {
	return &BadgerOption{
		Path:     path,
		DbName:   dbName,
		ClearOld: clearOld,
	}
}

func (option *BadgerOption) WithClearOld(b bool) *BadgerOption {
	option.ClearOld = b
	return option
}

func (option *BadgerOption) WithPath(path string) *BadgerOption {
	option.Path = path
	return option
}

func (option *BadgerOption) WithName(name string) *BadgerOption {
	option.DbName = name
	return option
}

// WithSyncWrite 写硬盘模式
func (option *BadgerOption) WithSyncWrite(syncWrite bool) *BadgerOption {
	option.SyncWrite = syncWrite
	return option
}

//func Test(path string) {
//	db, _ := badger.Open(badger.DefaultOptions(path))
//	//if e1!=nil{
//	//
//	//}
//	defer db.Close()
//	_ = db.Update(func(txn *badger.Txn) error {
//		_ = txn.Set([]byte("a"), []byte("123"))
//		return nil
//	})
//}

func pathExists(path string) bool {
	s, err := os.Stat(path) //os.Stat获取文件信息
	if err != nil {
		return false
	}
	return s.IsDir()
}

// GetDb 获取数据库
func GetDb(dbName string) (dao *BadgerKvDao, e error) {
	value, ok := dbMap.Load(dbName)
	//db, ok := dbMap[dbName]
	if !ok {
		return nil, errors.New("kv库不存在")
	}
	_, ok1 := value.(*badger.DB)
	if ok1 {
		return &BadgerKvDao{
			name: dbName,
		}, nil
	}
	return nil, errors.New("kv库不存在")
}

// View 开启只读事务
//func (dao *BadgerKvDao) View(work MyWork) (result interface{}, e error) {
//	dbName := dao.name
//	value, ok := dbMap.Load(dbName)
//	//db, ok := dbMap[dbName]
//	if !ok {
//		return nil, errors.New("数据库不存在")
//	}
//	db := value.(*badger.DB)
//	err := db.View(func(txn *badger.Txn) error {
//		myTx := MyTx{
//			Txn: txn,
//		}
//		var ex error
//		result, ex = work(&myTx)
//		if ex != nil {
//			return ex
//		}
//		return nil
//	})
//
//	if err != nil {
//		return nil, err
//	}
//
//	return
//}

func (myTx *HashKvTx) KeysByPrefix(prefix string, page int, pageSize int) ([]string, error) {

	if page < 1 {
		page = 1
	}
	//key的偏移数量
	offset := (page - 1) * pageSize
	var keyArr []string
	txn := myTx.Txn
	opts := badger.DefaultIteratorOptions
	opts.PrefetchValues = false
	it := txn.NewIterator(opts)

	defer it.Close()
	i := 0
	for it.Rewind(); it.ValidForPrefix([]byte(prefix)); it.Next() {
		item := it.Item()
		if i < offset {
			i++
			continue
		}
		if i < offset+pageSize {
			k := item.Key()
			keyArr = append(keyArr, string(k))
		} else {
			break
		}
		i++
	}

	return keyArr, nil
}

// NewHashKvReadTx 开启一个只读事务
func (dao *BadgerKvDao) NewHashKvReadTx() (*HashKvTx, error) {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	value, ok := dbMap.Load(dbName)
	if !ok {
		return nil, errors.New("数据库不存在")
	}
	db := value.(*badger.DB)
	txn := db.NewTransaction(false)
	return &HashKvTx{
		Txn: txn,
	}, nil
}

// NewHashKvWriteTx 开启一个读写事务
func (dao *BadgerKvDao) NewHashKvWriteTx() (*HashKvTx, error) {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	value, ok := dbMap.Load(dbName)
	if !ok {
		return nil, errors.New("数据库不存在")
	}
	db := value.(*badger.DB)
	txn := db.NewTransaction(true)
	return &HashKvTx{
		Txn: txn,
	}, nil
}

// Discard 对于只读事务，调用 当前函数就足够结束事务了,一般在defer中调用
func (myTx *HashKvTx) Discard() {
	myTx.Txn.Discard()
}

// Commit 对于读写事务，调用当前函数就能提交事务开始到现在的所有操作
// 但是，如果您的代码由于某种原因没有调用 当前函数（例如，它过早地返回错误），那么请确保您的Discard()在一个defer块中调用。请参考下面的代码。
// myTx:=NewHashKvReadTx()
// defer myTx.Discard
func (myTx *HashKvTx) Commit() {
	myTx.Txn.Commit()
}

// KeysByPrefix 前缀查询key
func (dao *BadgerKvDao) KeysByPrefix(prefix string, page int, pageSize int) ([]string, error) {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	value, ok := dbMap.Load(dbName)
	if !ok {
		return nil, errors.New("数据库不存在")
	}
	db := value.(*badger.DB)
	if page < 1 {
		page = 1
	}
	//key的偏移数量
	offset := (page - 1) * pageSize
	var keyArr []string
	err := db.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchValues = false
		it := txn.NewIterator(opts)

		defer it.Close()
		i := 0
		for it.Rewind(); it.ValidForPrefix([]byte(prefix)); it.Next() {
			item := it.Item()
			if i < offset {
				i++
				continue
			}
			if i < offset+pageSize {
				k := item.Key()
				keyArr = append(keyArr, string(k))
			} else {
				break
			}
			i++
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return keyArr, nil
}

// KeysByKeyWords 获取关键字的key集合
func (myTx *HashKvTx) KeysByKeyWords(keyword string, page int, pageSize int) ([]string, error) {

	if page < 1 {
		page = 1
	}
	//key的偏移数量
	offset := (page - 1) * pageSize
	var keyArr []string

	txn := myTx.Txn
	opts := badger.DefaultIteratorOptions
	opts.PrefetchValues = false
	it := txn.NewIterator(opts)
	defer it.Close()

	i := 0
	for it.Rewind(); it.Valid(); it.Next() {
		item := it.Item()
		k := string(item.Key())
		if !strings.Contains(k, keyword) {
			continue
		}
		if i < offset {
			i++
			continue
		}
		if i < offset+pageSize {
			keyArr = append(keyArr, k)
		} else {
			break
		}
		i++
	}

	return keyArr, nil
}

// GetSequence 获取一个基于kv的整数增长序列，参数bandwidth为序列分布，太密集的分布不利于kv的存储和读写
func (dao *BadgerKvDao) GetSequence(key []byte, bandwidth uint64) (*badger.Sequence, error) {
	dbName := dao.name
	value, ok := dbMap.Load(dbName)
	if !ok {
		return nil, errors.New("数据库不存在")
	}
	db := value.(*badger.DB)

	seq, e := db.GetSequence(key, bandwidth)
	return seq, e
}

// KeysByKeyWords 获取关键字的key集合
func (dao *BadgerKvDao) KeysByKeyWords(keyword string, page int, pageSize int) ([]string, error) {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	value, ok := dbMap.Load(dbName)
	if !ok {
		return nil, errors.New("数据库不存在")
	}
	db := value.(*badger.DB)
	if page < 1 {
		page = 1
	}
	//key的偏移数量
	offset := (page - 1) * pageSize
	var keyArr []string
	err := db.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchValues = false
		it := txn.NewIterator(opts)
		defer it.Close()

		i := 0
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			k := string(item.Key())
			if !strings.Contains(k, keyword) {
				continue
			}
			if i < offset {
				i++
				continue
			}
			if i < offset+pageSize {
				keyArr = append(keyArr, k)
			} else {
				break
			}
			i++
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return keyArr, nil
}

func (myTx *HashKvTx) Keys(page int, pageSize int) ([]string, error) {

	if page < 1 {
		page = 1
	}
	//key的偏移数量
	offset := (page - 1) * pageSize
	var keyArr []string
	txn := myTx.Txn

	opts := badger.DefaultIteratorOptions
	opts.PrefetchValues = false
	it := txn.NewIterator(opts)
	defer it.Close()
	i := 0
	for it.Rewind(); it.Valid(); it.Next() {
		item := it.Item()
		if i < offset {
			i++
			continue
		}
		if i < offset+pageSize {
			k := item.Key()
			keyArr = append(keyArr, string(k))
		} else {
			break
		}
		i++
	}

	return keyArr, nil
}

func (dao *BadgerKvDao) KeysV1(startKey string, pageSize int) ([]string, error) {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	value, ok := dbMap.Load(dbName)
	if !ok {
		return nil, errors.New("数据库不存在")
	}
	db := value.(*badger.DB)

	var keyArr []string
	err := db.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchValues = false
		it := txn.NewIterator(opts)
		defer it.Close()
		for it.Seek([]byte(startKey)); it.Valid(); it.Next() {
			item := it.Item()
			k := item.Key()
			keyArr = append(keyArr, string(k))
			// 如果达到了页面大小，就停止迭代
			if len(keyArr) >= pageSize {
				break
			}
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return keyArr, nil
}

func (dao *BadgerKvDao) Keys(page int, pageSize int) ([]string, error) {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	value, ok := dbMap.Load(dbName)
	if !ok {
		return nil, errors.New("数据库不存在")
	}
	db := value.(*badger.DB)
	if page < 1 {
		page = 1
	}
	//key的偏移数量
	offset := (page - 1) * pageSize
	var keyArr []string
	err := db.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchValues = false
		it := txn.NewIterator(opts)
		defer it.Close()
		i := 0

		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			if i < offset {
				i++
				continue
			}
			if i < offset+pageSize {
				k := item.Key()
				keyArr = append(keyArr, string(k))
			} else {
				break
			}
			i++
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return keyArr, nil
}

func (myTx *HashKvTx) AllKeys() []string {

	var keyArr []string

	txn := myTx.Txn
	opts := badger.DefaultIteratorOptions
	opts.PrefetchValues = false
	it := txn.NewIterator(opts)
	defer it.Close()
	for it.Rewind(); it.Valid(); it.Next() {
		item := it.Item()
		k := item.Key()
		keyArr = append(keyArr, string(k))
	}

	return keyArr
}

func (dao *BadgerKvDao) AllKeys() ([]string, error) {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	value, ok := dbMap.Load(dbName)
	if !ok {
		return nil, errors.New("数据库不存在")
	}
	db := value.(*badger.DB)
	var keyArr []string
	err := db.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchValues = false
		it := txn.NewIterator(opts)
		defer it.Close()
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			k := item.Key()
			keyArr = append(keyArr, string(k))
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return keyArr, nil
}

// Remove 删除key
func (myTx *HashKvTx) Remove(key string) error {

	txn := myTx.Txn
	err := txn.Delete([]byte(key))

	if err != nil {
		return err
	}
	return nil
}

// Remove 删除key
func (dao *BadgerKvDao) Remove(key string) error {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	value, ok := dbMap.Load(dbName)
	if !ok {
		return errors.New("数据库不存在")
	}
	db := value.(*badger.DB)
	err := db.Update(func(txn *badger.Txn) error {
		err := txn.Delete([]byte(key))
		return err
	})
	if err != nil {
		return err
	}
	return nil
}

// Save 保存kv
func (myTx *HashKvTx) Save(key string, value []byte) error {

	txn := myTx.Txn
	err := txn.Set([]byte(key), value)

	if err != nil {
		return err
	}
	return nil
}

func (dao *BadgerKvDao) GetTopEntry(pageSize int, sort string) (entrys []*badger.Entry, ex error) {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	v, ok := dbMap.Load(dbName)
	if !ok {
		return nil, errors.New("数据库不存在")
	}
	db := v.(*badger.DB)
	err := db.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10
		if sort == SortDes {
			opts.Reverse = true //// 设置为反向迭代
		}
		it := txn.NewIterator(opts)
		defer it.Close()

		// 查找最小的键（FIFO）
		it.Seek([]byte{})
		if !it.Valid() {
			return errors.New("没有任何元素")
		}

		for i := 0; it.Valid() && i < pageSize; it.Next() {
			k := it.Item().Key()
			var value []byte
			e := it.Item().Value(func(val []byte) error {
				value = make([]byte, len(val))
				copy(value, val)
				return nil
			})
			if e != nil {
				return e
			}
			entry := badger.NewEntry(k, value)
			entrys = append(entrys, entry)
			i++
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return entrys, nil
}

func (dao *BadgerKvDao) GetTopKey(pageSize int, sort string) (keys [][]byte, ex error) {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	v, ok := dbMap.Load(dbName)
	if !ok {
		return nil, errors.New("数据库不存在")
	}
	db := v.(*badger.DB)
	err := db.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10
		opts.PrefetchValues = false
		if sort == SortDes {
			opts.Reverse = true //// 设置为反向迭代
		}
		it := txn.NewIterator(opts)
		defer it.Close()

		// 查找最小的键（FIFO）
		it.Seek([]byte{})
		if !it.Valid() {
			return errors.New("没有任何元素")
		}

		for i := 0; it.Valid() && i < pageSize; it.Next() {
			k := it.Item().Key()
			keys = append(keys, k)
			i++
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return keys, nil
}

// Save 保存kv
func (dao *BadgerKvDao) Save(key string, value []byte) error {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	v, ok := dbMap.Load(dbName)
	if !ok {
		return errors.New("数据库不存在")
	}
	db := v.(*badger.DB)
	err := db.Update(func(txn *badger.Txn) error {
		err := txn.Set([]byte(key), value)
		return err
	})
	if err != nil {
		return err
	}
	return nil
}

// SetNxWithTTL 如果key不存在，则保持，否则失败
func (dao *BadgerKvDao) SetNxWithTTL(key string, value []byte, second int64) (bool, error) {
	globalLock.Lock()
	defer globalLock.Unlock()
	dbName := dao.name
	//db, ok := dbMap[dbName]
	v, ok := dbMap.Load(dbName)
	if !ok {
		return false, errors.New("数据库不存在")
	}
	db := v.(*badger.DB)

	txn := db.NewTransaction(true)
	defer txn.Discard()

	item, _ := txn.Get([]byte(key))
	if item != nil {
		//数据存在,则无法保存成功
		return false, nil
	}

	entry := badger.NewEntry([]byte(key), value).WithTTL(time.Second * time.Duration(second))
	err2 := txn.SetEntry(entry)
	if err2 != nil {
		return false, err2
	}
	_ = txn.Commit()
	return true, nil
}

// SaveWithTTL 保存kv,并且指定过期时间，单位秒
func (myTx *HashKvTx) SaveWithTTL(key string, value []byte, second int64) error {
	txn := myTx.Txn

	entry := badger.NewEntry([]byte(key), value).WithTTL(time.Second * time.Duration(second))
	err := txn.SetEntry(entry)

	if err != nil {
		return err
	}
	return nil
}

func (myTx *HashKvTx) GetTopEntry(pageSize int, sort string) (entrys []*badger.Entry, ex error) {
	txn := myTx.Txn

	opts := badger.DefaultIteratorOptions
	opts.PrefetchSize = 10
	if sort == SortDes {
		opts.Reverse = true //// 设置为反向迭代
	}
	it := txn.NewIterator(opts)
	defer it.Close()

	// 查找最小的键（FIFO）
	it.Seek([]byte{})
	if !it.Valid() {
		return nil, errors.New("没有任何元素")
	}

	for i := 0; it.Valid() && i < pageSize; it.Next() {
		k := it.Item().Key()
		var value []byte
		e := it.Item().Value(func(val []byte) error {
			value = make([]byte, len(val))
			copy(value, val)
			return nil
		})
		if e != nil {
			return nil, e
		}
		entry := badger.NewEntry(k, value)
		entrys = append(entrys, entry)
		i++
	}

	return entrys, nil

}

func (myTx *HashKvTx) GetTopKey(pageSize int, sort string) (keys [][]byte, ex error) {
	txn := myTx.Txn
	opts := badger.DefaultIteratorOptions
	opts.PrefetchSize = 10
	opts.PrefetchValues = false
	if sort == SortDes {
		opts.Reverse = true //// 设置为反向迭代
	}
	it := txn.NewIterator(opts)
	defer it.Close()
	if !it.Valid() {
		return nil, errors.New("没有任何元素")
	}

	for i := 0; it.Valid() && i < pageSize; it.Next() {
		k := it.Item().Key()
		keys = append(keys, k)
		i++
	}
	return keys, nil
}

// SaveWithTTL 保存kv,并且指定过期时间，单位秒
func (dao *BadgerKvDao) SaveWithTTL(key string, value []byte, second int64) error {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	v, ok := dbMap.Load(dbName)
	if !ok {
		return errors.New("数据库不存在")
	}
	db := v.(*badger.DB)
	err := db.Update(func(txn *badger.Txn) error {
		entry := badger.NewEntry([]byte(key), value).WithTTL(time.Second * time.Duration(second))
		err := txn.SetEntry(entry)
		return err
	})
	if err != nil {
		return err
	}
	return nil
}

// SaveMeta 以json格式保存对象
func (myTx *HashKvTx) SaveMeta(key string, value interface{}) error {

	valueJson, e1 := json.Marshal(value)
	if e1 != nil {
		return e1
	}

	txn := myTx.Txn

	err := txn.Set([]byte(key), valueJson)

	if err != nil {
		return err
	}
	return nil
}

// SaveMeta 以json格式保存对象
func (dao *BadgerKvDao) SaveMeta(key string, value interface{}) error {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	v, ok := dbMap.Load(dbName)
	if !ok {
		return errors.New("数据库不存在")
	}
	db := v.(*badger.DB)
	valueJson, e1 := json.Marshal(value)
	if e1 != nil {
		return e1
	}

	err := db.Update(func(txn *badger.Txn) error {
		err := txn.Set([]byte(key), valueJson)
		return err
	})
	if err != nil {
		return err
	}
	return nil
}

// SaveMetaWithTTL 以json格式保存对象,并且指定过期时间，单位秒
func (myTx *HashKvTx) SaveMetaWithTTL(key string, value interface{}, second int64) error {

	valueJson, e1 := json.Marshal(value)
	if e1 != nil {
		return e1
	}

	txn := myTx.Txn

	entry := badger.NewEntry([]byte(key), valueJson).WithTTL(time.Second * time.Duration(second))
	err := txn.SetEntry(entry)

	if err != nil {
		return err
	}
	return nil
}

// SaveMetaWithTTL 以json格式保存对象,并且指定过期时间，单位秒
func (dao *BadgerKvDao) SaveMetaWithTTL(key string, value interface{}, second int64) error {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	v, ok := dbMap.Load(dbName)
	if !ok {
		return errors.New("数据库不存在")
	}
	db := v.(*badger.DB)
	valueJson, e1 := json.Marshal(value)
	if e1 != nil {
		return e1
	}

	err := db.Update(func(txn *badger.Txn) error {
		entry := badger.NewEntry([]byte(key), valueJson).WithTTL(time.Second * time.Duration(second))
		err := txn.SetEntry(entry)
		return err
	})
	if err != nil {
		return err
	}
	return nil
}

// GetMeta 根据key查找value对象
func (myTx *HashKvTx) GetMeta(key string, result interface{}) error {

	var value []byte

	txn := myTx.Txn
	var err2 error
	item, err1 := txn.Get([]byte(key))
	if err1 != nil {
		return err1
	}
	value, err2 = item.ValueCopy(value)
	if err2 != nil {
		return err2
	}

	err3 := json.Unmarshal(value, result)
	if err3 != nil {
		return err3
	}
	return nil
}

// GetMeta 根据key查找value对象
func (dao *BadgerKvDao) GetMeta(key string, result interface{}) error {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	v, ok := dbMap.Load(dbName)
	if !ok {
		return errors.New("数据库不存在")
	}
	db := v.(*badger.DB)
	var value []byte
	err := db.View(func(txn *badger.Txn) error {
		var err2 error
		item, err1 := txn.Get([]byte(key))
		if err1 != nil {
			return err1
		}
		value, err2 = item.ValueCopy(value)
		if err2 != nil {
			return err2
		}

		return nil
	})
	if err != nil {
		return err
	}
	err1 := json.Unmarshal(value, result)
	if err1 != nil {
		return err1
	}
	return nil
}

// Has 是否存在当前key
func (myTx *HashKvTx) Has(key string) (bool, error) {

	txn := myTx.Txn
	_, err1 := txn.Get([]byte(key))
	if err1 != nil {
		return false, err1
	}

	return true, nil
}

// Has 是否存在当前key
func (dao *BadgerKvDao) Has(key string) (bool, error) {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	v, ok := dbMap.Load(dbName)
	if !ok {
		return false, errors.New("数据库不存在")
	}
	db := v.(*badger.DB)
	err := db.View(func(txn *badger.Txn) error {

		_, err1 := txn.Get([]byte(key))
		if err1 != nil {
			return err1
		}
		return nil
	})
	if err != nil {
		return false, err
	}
	return true, nil
}

// Get 根据key查找value
func (myTx *HashKvTx) Get(key []byte) ([]byte, error) {

	var value []byte
	txn := myTx.Txn

	var err2 error
	item, err1 := txn.Get(key)
	if err1 != nil {
		return nil, err1
	}
	value, err2 = item.ValueCopy(value)
	if err2 != nil {
		return nil, err2
	}

	return value, nil
}

// DropAll 删除所有
func (dao *BadgerKvDao) DropAll() error {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	v, ok := dbMap.Load(dbName)
	if !ok {
		return errors.New("数据库不存在")
	}
	db := v.(*badger.DB)
	e := db.DropAll()
	if e != nil {
		return e
	}
	return nil
}

// Sync 同步数据到硬盘
func (dao *BadgerKvDao) Sync() error {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	v, ok := dbMap.Load(dbName)
	if !ok {
		return errors.New("数据库不存在")
	}
	db := v.(*badger.DB)
	e := db.Sync()
	if e != nil {
		return e
	}
	return nil
}

// Get 根据key查找value
func (dao *BadgerKvDao) Get(key []byte) ([]byte, error) {
	dbName := dao.name
	//db, ok := dbMap[dbName]
	v, ok := dbMap.Load(dbName)
	if !ok {
		return nil, errors.New("数据库不存在")
	}
	db := v.(*badger.DB)
	var value []byte
	err := db.View(func(txn *badger.Txn) error {
		var err2 error
		item, err1 := txn.Get(key)
		if err1 != nil {
			return err1
		}
		value, err2 = item.ValueCopy(value)
		if err2 != nil {
			return err2
		}

		return nil
	})
	if err != nil {
		return nil, err
	}
	return value, nil
}

// GetData 根据key查找value
func (myTx *HashKvTx) GetData(key string) ([]byte, error) {
	return myTx.Get([]byte(key))
}

// GetData 根据key查找value
func (dao *BadgerKvDao) GetData(key string) ([]byte, error) {
	return dao.Get([]byte(key))
}

func (dao *BadgerKvDao) Close() {

	dbName := dao.name
	var db *badger.DB
	value, ok := dbMap.Load(dbName)

	if ok {
		db1, ok1 := value.(*badger.DB)
		if ok1 {
			db = db1
			_ = db.Close()
			dbMap.Delete(dbName)
		}
	}

}

// DbNames 获取库名列表
func DbNames() []string {
	var dbNames []string

	dbMap.Range(func(key, v interface{}) bool {
		k := key.(string)
		dbNames = append(dbNames, k)
		return true
	})

	return dbNames
}

// NewMemory 新建内存kv
func NewMemory() (dao *BadgerKvDao, e error) {
	dbName := "terry.badger.db.mem"
	var db *badger.DB
	value, ok := dbMap.Load(dbName)

	if ok {
		db1, ok1 := value.(*badger.DB)
		if ok1 {
			db = db1
			return &BadgerKvDao{
				name: dbName,
			}, nil
		}
	}
	db, err1 := badger.Open(badger.DefaultOptions("").WithInMemory(true))
	if err1 != nil {
		return nil, err1
	}
	dbMap.Store(dbName, db)
	return &BadgerKvDao{
		path: "",
		name: dbName,
	}, nil
}

// New 新建一个kv对象
func New(option *BadgerOption) (dao *BadgerKvDao, e error) {
	kvPath := option.Path + "/" + option.DbName
	dbName := option.DbName
	//if dbMap == nil {
	//	dbMap = make(map[string]*badger.DB, 0)
	//}
	//库已经存在，则直接结束，重新构建
	//db, ok := dbMap[option.DbName]
	value, ok := dbMap.Load(dbName)
	if ok {
		_, ok1 := value.(*badger.DB)
		if ok1 {
			if option.ClearOld {
				dbMap.Delete(dbName)
			} else {
				return &BadgerKvDao{
					path: option.Path,
					name: option.DbName,
				}, nil
			}
		}

	}

	if option.ClearOld {
		if pathExists(option.Path) {
			//目录存在，则删了重建，保证每次启动都是全新的库
			_ = os.RemoveAll(kvPath)
		}
		//_ = os.MkdirAll(option.Path+"/"+option.DbName, os.ModePerm)
	}

	if !pathExists(kvPath) {
		//目录存在，则删了重建，保证每次启动都是全新的库
		_ = os.MkdirAll(kvPath, os.ModePerm)
	}
	badgerOption := badger.DefaultOptions(kvPath).WithSyncWrites(option.SyncWrite)

	db, err1 := badger.Open(badgerOption)
	if err1 != nil {
		return nil, err1
	}

	dbMap.Store(dbName, db)

	return &BadgerKvDao{
		path: option.Path,
		name: option.DbName,
	}, nil
}

// CloseAll 关闭数据库
func CloseAll() {

	dbMap.Range(func(key, v interface{}) bool {
		db := v.(*badger.DB)
		_ = db.Close()
		dbMap.Delete(key)
		return true
	})

}

func CloseDb(name string) {

	dbName := name
	var db *badger.DB
	value, ok := dbMap.Load(dbName)

	if ok {
		db1, ok1 := value.(*badger.DB)
		if ok1 {
			db = db1
			_ = db.Close()
			dbMap.Delete(dbName)
		}
	}

}
