package hash_badger

import (
	"fmt"
	"testing"
)

func TestKvCreate(t *testing.T) {
	kv, e := NewMemory()
	if e != nil {
		fmt.Println(e.Error())
		return
	}
	e = kv.Save("aa", []byte("11"))
	if e != nil {
		fmt.Println(e.Error())
		return
	}
}
